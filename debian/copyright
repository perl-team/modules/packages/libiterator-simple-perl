Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Iterator-Simple
Upstream-Contact: Michael Roberts <michael@cpan.org>
Upstream-Name: Iterator-Simple

Files: *
Copyright: 2007, 2008, Rintaro Ishizaki <rintaro@cpan.org>
 2013, 2017, Michael Roberts <michael@cpan.org>
License: Artistic or GPL-1+
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

Files: inc/Module/*
Copyright: 2003, 2004, Autrijus Tang <autrijus@autrijus.org>
 2002, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+
Comment: information obtained using `debsnap libmodule-install-perl 0.67-1`

Files: debian/*
Copyright: 2019, Sean Whitton <spwhitton@spwhitton.name>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
